/*
	JSON Objects
		-JSON stands for JavaScript Object Notation
		-JSON can also be used in other programming languages
		-Do not confuse JavaScript objects with JSON Objects
		-JSON is used for serializing different data types into bytes
			-serialization - process of converting data types into series of bytes for easier transmission or transfer of information
			-bytes - are information that a computer process to perform different tasks
		-uses double qoutes for property name
		-syntax:
			{
				"propertyA" : "valueA",
				"propertyB" : "valueB"
			}
			{
				"name" : "Yoon",
				"birthday" : "March 9, 1993"
			}
*/

	//JSON Objects
	/*{
		"city" : "Quezon City",
		"province" : "Metro Manila",
		"country" : "Philippines"
	};

	//JSON Arrays
	"cities" : [
		{
			"city" : "Quezon City",
			"province" : "Metro Manila",
			"country" : "Philippines"
		},
		{
			"city" : "Cebu City",
			"province" : "Cebu",
			"country" : "Philippines"
		}
	];*/

	//JSON Methods
		//-JSON Objects contain methods for parsing and converting data into stringified JSON
	let batchesArr = [
		{batchName: "Batch 169"},
		{batchName: "Batch 170"}
	];

	console.log(JSON.stringify(batchesArr));

	let data = JSON.stringify({
		name: "Luke",
		age: 45,
		address: {
			city: "Manila",
			country: "Philippines"
		}
	});
	console.log(data);

	//using Stringify method with variables

	//user details
	/*let firstName = prompt("First Name:");
	let lastName = prompt("Last Name:");
	let age = prompt("Age:");
	let address = {
		city: prompt("City"),
		country: prompt("Country")
	};

	let data2 = JSON.stringify({
		firstName : firstName,
		lastName : lastName,
		age : age,
		address : address
	})
	console.log(data2);*/


	//JSON.parse - to convert JSON to javascript
	let batchesJSON = `[
		{"batchName" : "Batch 169"},
		{"batchName" : "Batch 170"}
	]`

	console.log(JSON.parse(batchesJSON));